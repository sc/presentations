Here we collect our presentations.
You may commit your works directly to the `master` branch.

```
git clone git@git.app.uib.no:sc/presentations.git
cd presentations/
cp -p /path/to/your/file.pdf ./
git add file.pdf
git commit -a
git push
```

Only other modifications (reorganising or editting presentations that
are not your own) must go through a [pull
request](https://git.app.uib.no/sc/internal-documentation#writing-documentation).
